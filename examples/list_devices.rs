use easy_usb::PortableDevices;

fn main() {
    //Devices::enumerate();
    let pd = PortableDevices::devices();

    if let Ok(pd) = pd {
        for d in pd.devices.iter() {
            println!("{:?}", d);
        }
    }
}
